$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
   const gDATA_COLUMN = [
     "orderCode", 
     "kichCo", 
     "loaiPizza", 
     "idLoaiNuocUong", 
     "thanhTien", 
     "hoTen", 
     "soDienThoai", 
     "trangThai", 
     "action"
   ];

   const gORDER_CODE = 0;
   const gKICH_CO_COMBO = 1;
   const gLOAI_PIZZA = 2;
   const gNUOC_UONG = 3;
   const gTHANH_TIEN = 4;
   const gHO_VA_TEN = 5;
   const gSO_DIEN_THOAI = 6;
   const gTRANG_THAI = 7;
   const gACTION = 8;

   $("#user-table").DataTable({
     "columns": [
          {"data": gDATA_COLUMN[gORDER_CODE]},
          {"data": gDATA_COLUMN[gKICH_CO_COMBO]},
          {"data": gDATA_COLUMN[gLOAI_PIZZA]},
          {"data": gDATA_COLUMN[gNUOC_UONG]},
          {"data": gDATA_COLUMN[gTHANH_TIEN]},
          {"data": gDATA_COLUMN[gHO_VA_TEN]},
          {"data": gDATA_COLUMN[gSO_DIEN_THOAI]},
          {"data": gDATA_COLUMN[gTRANG_THAI]},
          {"data": gDATA_COLUMN[gACTION]}

     ],
     columnDefs: [
          {
               targets: gACTION,
               defaultContent: "<button class = 'btn btn-primary btn-detail'>Chi tiết</button>"
          }
     ]
   });

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

$(document).on("click", ".btn-detail", function(){
          onBtnDetailClick(this);
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm xử lý sự kiện load trang
function onPageLoading(){
     "use strict";
     $.ajax({
          url:"http://203.171.20.210:8080/devcamp-pizza365/orders",
          type: "GET", 
          dataType: "json", 
          success: function(res){
               loadDataToTable(res);
          }, 
          error: function(err){
               alert(err.responseText);
          }    
     });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
     //hàm load dữ liệu vào table
     function loadDataToTable(paramUserObj){
          "use strict";
          var vTable = $("#user-table").DataTable(); //tạo biến truy xuất đến datatable
          vTable.clear();
          vTable.rows.add(paramUserObj);
          vTable.draw();
     }

     //hàm xử lý sự kiện bấm nút chi tiết
     function onBtnDetailClick(paramEle){
          "use strict";
          var vRowClick = $(paramEle).parents("tr"); //xác định parent tr chứa nút bấm click
          var vTable = $("#user-table").DataTable(); //tạo biến truy xuất đến datatable
          var vDataRow = vTable.row(vRowClick).data(); //lấy dữ liệu của hangf dữ liệu chứa nút bấm 
          
     }

});